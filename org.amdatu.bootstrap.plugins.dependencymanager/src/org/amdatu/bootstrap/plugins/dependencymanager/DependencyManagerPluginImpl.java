/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.dependencymanager;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.FullyQualifiedName;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Dependency;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.bootstrap.plugins.dependencymanager.api.DependencyManagerPlugin;
import org.amdatu.bootstrap.plugins.dependencymanager.api.ServiceDependencyDescription;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.HeaderClause;
import aQute.bnd.header.Attrs;
import aQute.bnd.properties.Document;

@Component(provides = { Plugin.class, DependencyManagerPlugin.class })
public class DependencyManagerPluginImpl extends AbstractBasePlugin implements DependencyManagerPlugin {
	private static final String ANNOTATION_PLUGIN_NAME = "org.apache.felix.dm.annotation.plugin.bnd.AnnotationPlugin";

	private static final String ANNOTATION_PROCESSOR_JAR = "org.apache.felix.dependencymanager.annotation-3.1.1-SNAPSHOT.jar";

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile Prompt m_prompt;

	@ServiceDependency
	private volatile ResourceManager m_resourceManager;

	@ServiceDependency(required = true)
	private volatile TemplateEngine m_templateEngine;

	private volatile BundleContext m_bundleContext;

	@Override
	public void install() {
		boolean useAnnotations = m_prompt.askBoolean("Do you want to use annotations?", true);
		if (useAnnotations) {
			installAnnotationProcessor();
			
			m_dependencyBuilder.addDependency("../cnf/plugins/org.apache.felix.dependencymanager.annotation-3.1.1-SNAPSHOT.jar", "file");
		}

	}

	@Command
	public void addComponent() {
		String registrationType = m_prompt.askChoice("How do you want to register the component?", 0,
				Arrays.asList("Annotations", "Activator"));

		FullyQualifiedName fqn = m_prompt.askComponentName();

		String interfaceChoice = m_prompt.askChoice("What interface do you want to publish?", 0,
				Arrays.asList("None", "java.lang.Object", "other"));

		String interfaceName = null;
		if (interfaceChoice.equals("other")) {
			interfaceName = m_prompt.askString("What is the fully qualified name of the interface?");
		}

		URL templateUri;
		if (registrationType.equals("Annotations")) {
			templateUri = m_bundleContext.getBundle().getEntry("/templates/annotatedcomponent.vm");
		} else {
			templateUri = m_bundleContext.getBundle().getEntry("/templates/component.vm");
		}

		String activatorInterfaceName = null;
		if (interfaceName != null) {
			activatorInterfaceName = interfaceChoice;
		} else if (interfaceChoice.equals("java.lang.Object")) {
			activatorInterfaceName = "java.lang.Object";
		}

		FullyQualifiedName interfaceFullyQualifiedName = null;
		if(interfaceName != null)
			interfaceFullyQualifiedName = new FullyQualifiedName(interfaceName);
			
		createComponentFile(fqn.getClassName() + ".java", fqn, interfaceFullyQualifiedName, templateUri);

		if (registrationType.equals("Activator")) {
			createActivator(fqn, new FullyQualifiedName(activatorInterfaceName));
		}
	}

	private void createComponentFile(String fileName, FullyQualifiedName fqn, FullyQualifiedName interfaceName, URL templateUri, ServiceDependencyDescription... dependencies) {
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("componentName", fqn.getClassName());
			context.put("interfaceName", interfaceName);
			context.put("packageName", fqn.getPackageName());
			context.put("dependencies", dependencies);

			String template = processor.generateString(context);
			Path outputDir = m_navigator.getCurrentDir().resolve("src").resolve(fqn.getPackageName().replaceAll("\\.", "/"));
			Path outputFile = outputDir.resolve(fileName);
			Files.createDirectories(outputDir);

			System.out.println("Created file: " + outputFile);
			Files.write(outputFile, template.getBytes());

		} catch (TemplateException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void installAnnotationProcessor() {
		Path mainBnd = m_navigator.getWorkspaceDir().resolve("cnf/build.bnd");

		try {
			String mainBndContents = new String(Files.readAllBytes(mainBnd));

			if (!mainBndContents.contains(ANNOTATION_PLUGIN_NAME)) {
				BndEditModel model = new BndEditModel();
				model.loadFrom(mainBnd.toFile());

				List<HeaderClause> plugins = model.getPlugins();
				if (plugins == null) {
					plugins = new ArrayList<>();
				}

				if (plugins.isEmpty()) {
					plugins.add(createRepoPluginHeader());
				}

				plugins.add(createAnnotationPluginHeader());

				model.setPlugins(plugins);

				Document document = new Document(mainBndContents);
				model.saveChangesTo(document);

				m_resourceManager.writeFile(mainBnd, document.get().getBytes());

				Path pluginsDir = m_navigator.getWorkspaceDir().resolve("cnf/plugins");
				try (InputStream in = m_bundleContext.getBundle().getEntry("/libs/" + ANNOTATION_PROCESSOR_JAR).openStream()) {
					Files.copy(in, pluginsDir.resolve(ANNOTATION_PROCESSOR_JAR), StandardCopyOption.REPLACE_EXISTING);
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private HeaderClause createRepoPluginHeader() {
		Attrs pluginAttrs = new Attrs();
		HeaderClause header = new HeaderClause("${ext.repositories.-plugin}", pluginAttrs);
		return header;
	}

	private HeaderClause createAnnotationPluginHeader() {
		Attrs pluginAttrs = new Attrs();
		pluginAttrs.put("path:", "cnf/plugins/" + ANNOTATION_PROCESSOR_JAR);
		pluginAttrs.put("build-import-export-service", "false");
		HeaderClause header = new HeaderClause(ANNOTATION_PLUGIN_NAME, pluginAttrs);
		return header;
	}

	@Override
	public String getName() {
		return "dependencymanager";
	}

	@Override
	public void createActivator(FullyQualifiedName fqn, FullyQualifiedName interfaceName, ServiceDependencyDescription... dependencies) {
		URL activatorTemplate = m_bundleContext.getBundle().getEntry("/templates/activator.vm");
		createComponentFile("Activator.java", fqn, interfaceName, activatorTemplate, dependencies);
		String activator = "\nBundle-Activator: " + fqn.getPackageName() + ".Activator";
		
		try {
			Files.write(m_navigator.getBndFile(), activator.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Collection<Dependency> getDependencies() {
		return Dependency.fromStrings("org.apache.felix.dependencymanager", "osgi.core");
	}

	@Override
	public boolean isInstalled() {
		return m_dependencyBuilder.hasDependencies(getDependencies()) && annotationProcessorInstalled();
	}

	private boolean annotationProcessorInstalled() {
		try {
			Path mainBnd = m_navigator.getWorkspaceDir().resolve("cnf/build.bnd");
			String mainBndContents = new String(Files.readAllBytes(mainBnd));
			return mainBndContents.contains(ANNOTATION_PLUGIN_NAME);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}

	}

	@Override
	public Collection<Dependency> getRunRequirements() {
		return Dependency.fromStrings("org.apache.felix.dependencymanager", 
				"org.apache.felix.dependencymanager.runtime",
				"org.apache.felix.dependencymanager.shell",
				"org.apache.felix.metatype",
				"org.apache.felix.eventadmin",
				"org.apache.felix.configadmin",
				"org.apache.felix.log");
	}
}
