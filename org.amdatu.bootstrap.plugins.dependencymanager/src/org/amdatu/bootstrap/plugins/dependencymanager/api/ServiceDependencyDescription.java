/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.dependencymanager.api;

import org.amdatu.bootstrap.core.api.FullyQualifiedName;

public class ServiceDependencyDescription {
	private final FullyQualifiedName m_fqn;
	private final boolean m_required;

	public ServiceDependencyDescription(String fqn, boolean required) {
		m_fqn = new FullyQualifiedName(fqn);
		m_required = required;
	}

	public ServiceDependencyDescription(String fqn) {
		m_fqn = new FullyQualifiedName(fqn);
		m_required = false;
	}

	public FullyQualifiedName getFqn() {
		return m_fqn;
	}

	public boolean isRequired() {
		return m_required;
	}

	@Override
	public String toString() {
		return "ServiceDependency [m_fqn=" + m_fqn + ", m_required=" + m_required + "]";
	}
}
