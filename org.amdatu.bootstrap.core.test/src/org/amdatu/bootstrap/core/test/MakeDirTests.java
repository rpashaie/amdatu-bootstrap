package org.amdatu.bootstrap.core.test;

import org.amdatu.bootstrap.testframework.AbstractBaseTestCase;

public class MakeDirTests extends AbstractBaseTestCase{
	
	public void testMkDir() throws Exception {
		assertFalse(m_navigator.getCurrentDir().resolve("mytest").toFile().exists());
		
		runScript("core:mkdir mytest");
		
		assertTrue(m_navigator.getCurrentDir().resolve("mytest").toFile().exists());
	}
}
