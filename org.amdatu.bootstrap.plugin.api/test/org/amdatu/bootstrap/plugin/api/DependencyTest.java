/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugin.api;

import junit.framework.TestCase;

/**
 * Test cases for {@link Dependency}.
 */
public class DependencyTest extends TestCase {

    public void testCreateFromStringWithBsnAndArgsOk() throws Exception {
        Dependency dep = new Dependency("org.osgi.core; arg1 = value1 ; version = 1.2.3");
        assertEquals(dep, "org.osgi.core", "1.2.3");
    }

    public void testCreateFromStringWithBsnAndVersionOk() throws Exception {
        Dependency dep = new Dependency("org.osgi.core; version = 1.2.3");
        assertEquals(dep, "org.osgi.core", "1.2.3");
    }

    public void testCreateFromStringWithBsnOnlyOk() throws Exception {
        Dependency dep = new Dependency("org.osgi.core");
        assertEquals(dep, "org.osgi.core", null);
    }

    public void testCreateWithBsnAndVersionOk() throws Exception {
        Dependency dep = new Dependency("org.osgi.core", "1.2.3");
        assertEquals(dep, "org.osgi.core", "1.2.3");
    }

    public void testCreateWithBsnAndNullVersionOk() throws Exception {
        Dependency dep = new Dependency("org.osgi.core", null);
        assertEquals(dep, "org.osgi.core", null);
    }

    public void testCreateWithBsnAndEmptyVersionOk() throws Exception {
        Dependency dep = new Dependency("org.osgi.core", "  ");
        assertEquals(dep, "org.osgi.core", null);
    }

    public void testCreateWithBsnWithArgumentsFail() throws Exception {
        try {
            new Dependency("foo;bar", "1.2.3");
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testCreateWithEmptyBsnFail() throws Exception {
        try {
            new Dependency("  ", "1.2.3");
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testCreateWithEmptyStringFail() throws Exception {
        try {
            new Dependency("  ");
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testCreateFromStringWithMultipleBSNsFail() throws Exception {
        try {
            new Dependency("foo;bar;version=1.2.3");
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testCreateFromStringWithMultipleVersionsFail() throws Exception {
        try {
            new Dependency("foo;version = 1.2.3 ; version = 3.4.5 ");
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testCreateFromStringWithIncompleteVersionsFail() throws Exception {
        try {
            new Dependency("foo;version");
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testCreateFromStringWithoutBsnFail() throws Exception {
        try {
            new Dependency("version = 3.4.5 ");
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testCreateWithNullBsnFail() throws Exception {
        try {
            new Dependency(null, "1.2.3");
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testCreateWithNullStringFail() throws Exception {
        try {
            new Dependency(null);
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    private void assertEquals(Dependency dep, String bsn, String version) {
        assertNotNull("Dependency cannot be null!", dep);
        assertEquals("BSN does not match", bsn, dep.getBsn());
        assertEquals("Version does not match", version, dep.getVersion());
    }
}
