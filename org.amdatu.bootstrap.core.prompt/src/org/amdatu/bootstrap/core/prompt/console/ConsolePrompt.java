package org.amdatu.bootstrap.core.prompt.console;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.amdatu.bootstrap.core.prompt.Prompt;

import com.sun.org.apache.bcel.internal.generic.NamedAndTyped;

import aQute.bnd.annotation.metatype.Meta;
import aQute.bnd.annotation.metatype.Meta.AD;
import aQute.lib.converter.Converter;
import aQute.lib.converter.Converter.Hook;

/** Console prompt, implemented as part of our Gogo shell commands. */
public class ConsolePrompt implements Prompt {
	private Converter m_converter;
	public ConsolePrompt() {
		m_converter = new Converter();
		m_converter.hook(FQN.class, new Hook() {
			@Override
			public Object convert(Type dest, Object o) throws Exception {
				if (o instanceof FQN) {
					return o;
				}
				return new FQN(o.toString());
			}});
	}

	@Override
	public <T> T ask(Class<T> questions, Map<String, Object> answers) {
		for (Method method : questions.getMethods()) {
			String name = method.getName();
			Object result = null;
			Object answer = answers.get(name);
			if (answer != null) {
				try {
					result = m_converter.convert(method.getReturnType(), answer);
				}
				catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			while (result == null) {
				AD ad = method.getAnnotation(Meta.AD.class);
				String description = ad.description();
				String def = ad.deflt();
				System.out.println(description);
				System.out.println(name + ":");
				Scanner scanner = new Scanner(System.in);
				String string = scanner.nextLine();
				try {
					if (string == null || string.trim().length() == 0) {
						result = m_converter.convert(method.getReturnType(), def);
					}
					else {
						result = m_converter.convert(method.getReturnType(), string);
					}
				}
				catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			answers.put(name, result);
		}
		return m_converter.proxy(questions, answers);
	}
}
