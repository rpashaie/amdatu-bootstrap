package org.amdatu.bootstrap.core.prompt.console;

public class FQN {
	private final String m_package;
	private final String m_class;
	
	public FQN(String name) {
		int index = name.lastIndexOf('.');
		if (index > 0) {
			m_package = name.substring(0, index);
			m_class = name.substring(index + 1);
		}
		else {
			throw new IllegalArgumentException("Cannot use the default package in OSGi.");
		}
	}
	
	public String getPackage() { return m_package; }
	public String getClassName() { return m_class; }
	@Override
	public String toString() {
		return "FQN[" + m_package + "." + m_class + "]";
	}
}