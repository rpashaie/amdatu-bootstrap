/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.amdatu.web;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Dependency;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.bootstrap.plugins.amdatu.web.api.WebResourcePlugin;
import org.amdatu.bootstrap.plugins.dependencymanager.api.DependencyManagerPlugin;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.VersionedClause;
import aQute.bnd.header.Attrs;
import aQute.bnd.properties.Document;

@Component(provides = { Plugin.class, WebResourcePlugin.class })
public class AmdatuWebResourcePlugin extends AbstractBasePlugin implements  WebResourcePlugin{
	@ServiceDependency
	private volatile Prompt m_prompt;

	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile ResourceManager m_resourceManager;

	@ServiceDependency
	private volatile DependencyManagerPlugin m_dmPlugin;
	
	@Command
	public void add() throws IOException {

		String resourcesDir = m_prompt.askString("Which directory in the project contains the static resources?", "static");
		String path = m_prompt.askString("Which path do you want to register on?", resourcesDir);
		String defaultPath = getDefaultPath(resourcesDir);
		
		File bndFile = m_prompt.askChoice("To which bundle do you want to add the handler?", 0, m_navigator.listProjectBndFiles());
		
		addWebResources(path, defaultPath, resourcesDir, bndFile);
	}

	@Override
	public void addWebResources(String path, String defaultPath, String resourcesDir, File bndFile) {
		if (!path.startsWith("/")) {
			path = "/" + path;
		}

		try {
			BndEditModel model = new BndEditModel();
			model.loadFrom(bndFile);

			model.addIncludeResource(resourcesDir + "=" + resourcesDir);
			model.genericSet("X-Web-Resource-Version", " 1.1");

			model.genericSet("X-Web-Resource", createWebResourceHeader(resourcesDir, path, model));

			if (!defaultPath.equals("None")) {
				model.genericSet("X-Web-Resource-Default-Page", " " + defaultPath);
			}

			String bndContents = new String(Files.readAllBytes(bndFile.toPath()));
			Document document = new Document(bndContents);
			model.saveChangesTo(document);

			m_resourceManager.writeFile(bndFile.toPath(), document.get().getBytes());
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Command
	public void addRunConfig() throws IOException {
		List<Path> runConfigs = m_navigator.findWorkspaceRunConfigs();
		Path path = m_prompt.askChoice("Which run config do you want to add to?", 0, runConfigs);

		BndEditModel model = new BndEditModel();

		model.loadFrom(path.toFile());

		List<VersionedClause> runBundles = model.getRunBundles();
		if (runBundles == null) {
			runBundles = new ArrayList<>();
		}

		addRunBundle("org.apache.felix.configadmin", runBundles);
		addRunBundle("org.apache.felix.dependencymanager", runBundles);
		addRunBundle("org.apache.felix.http.jetty", runBundles);
		addRunBundle("org.apache.felix.http.whiteboard", runBundles);
		addRunBundle("org.apache.felix.metatype", runBundles);

		model.setRunBundles(runBundles);

		String bndContents = new String(Files.readAllBytes(path));
		Document document = new Document(bndContents);
		model.saveChangesTo(document);

		m_resourceManager.writeFile(path, document.get().getBytes());
	}

	private void addRunBundle(String bsn, List<VersionedClause> runBundles) {
		for (VersionedClause versionedClause : runBundles) {
			if (versionedClause.getName().equals(bsn)) {
				System.out.println("Skipping " + bsn + " since it was already in the run config");
				return;
			}
		}

		runBundles.add(new VersionedClause(bsn, new Attrs()));
	}

	private String createWebResourceHeader(String resourcesDir, String path, BndEditModel model) {
		Object webResource = model.genericGet("X-Web-Resource");

		StringBuilder webResourceBuilder = new StringBuilder(" ");
		if (webResource != null) {
			webResourceBuilder.append(webResource).append(";");
		}

		webResourceBuilder.append(path);
		webResourceBuilder.append(";");
		webResourceBuilder.append(resourcesDir);
		return webResourceBuilder.toString();
	}

	private String getDefaultPath(String resourcesDir) {
		File[] staticFiles = m_navigator.getProjectDir().resolve(resourcesDir).toFile().listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.isFile();
			}
		});

		List<String> defaultFileOptions = new ArrayList<>();
		defaultFileOptions.add("None");
		for (File staticFile : staticFiles) {
			defaultFileOptions.add(staticFile.getName());
		}

		defaultFileOptions.add("Other");

		String defaultPath = m_prompt.askChoice("What is the default page?", 0, defaultFileOptions);
		if (defaultPath.equals("Other")) {
			defaultPath = m_prompt.askString("What is the default page?");
		}
		return defaultPath;
	}

	@Override
	public String getName() {
		return "webresources";
	}

	@Override
	public Collection<Dependency> getRunRequirements() {
		List<Dependency> requirements = Dependency.fromStrings("org.apache.felix.http.jetty", "org.apache.felix.http.whiteboard", "org.amdatu.web.resourcehandler");
		requirements.addAll(m_dmPlugin.getRunRequirements());
		return requirements;
	}

	@Override
	public boolean isInstalled() {
		return true;
	}
}
