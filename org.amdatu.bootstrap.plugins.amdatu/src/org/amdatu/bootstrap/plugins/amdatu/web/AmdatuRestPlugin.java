/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.amdatu.web;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.FullyQualifiedName;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Dependency;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.bootstrap.plugins.dependencymanager.api.DependencyManagerPlugin;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component(provides = Plugin.class)
public class AmdatuRestPlugin extends AbstractBasePlugin {
	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile Prompt m_prompt;

	@ServiceDependency
	private volatile ResourceManager m_resourceManager;

	@ServiceDependency(required = true)
	private volatile TemplateEngine m_templateEngine;

	@ServiceDependency
	private volatile DependencyManagerPlugin m_dependencyManagerPlugin;

	private volatile BundleContext m_bundleContext;

	@Command
	public void addComponent() throws TemplateException, IOException {
		String registrationType = m_prompt.askChoice("How do you want to register the component?", 0,
				Arrays.asList("Annotations", "Activator"));
		FullyQualifiedName fqn = m_prompt.askComponentName();
		String path = m_prompt.askString("On what path do you want to register the component?");

		boolean useAnnotations = registrationType.equals("Annotations");

		URL templateUri = m_bundleContext.getBundle().getEntry("/templates/restcomponent.vm");

		TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
		TemplateContext context = m_templateEngine.createContext();
		context.put("componentName", fqn.getClassName());
		context.put("packageName", fqn.getPackageName());
		context.put("useAnnotations", useAnnotations);
		context.put("path", path);

		String template = processor.generateString(context);
		Path outputDir = m_navigator.getCurrentDir().resolve("src").resolve(fqn.getPackageName().replaceAll("\\.", "/"));
		Path outputFile = outputDir.resolve(fqn.getClassName() + ".java");
		Files.createDirectories(outputDir);

		System.out.println("Created file: " + outputFile);
		Files.write(outputFile, template.getBytes());

		if (!useAnnotations) {
			m_dependencyManagerPlugin.createActivator(fqn, new FullyQualifiedName("java.lang.Object"));
		}
	}

	@Override
	public String getName() {
		return "rest";
	}

	@Override
	public Collection<Dependency> getRunRequirements() {
		List<Dependency> requirements = Dependency.fromStrings("org.apache.felix.http.jetty",
				"org.apache.felix.http.whiteboard", "org.amdatu.web.rest.jaxrs", "org.amdatu.web.rest.wink",
				"org.amdatu.web.rest.doc", "jackson-jaxrs", "jackson-core-asl", "jackson-mapper-asl");
		requirements.addAll(m_dependencyManagerPlugin.getRunRequirements());
		return requirements;
	}

	@Override
	public Collection<Dependency> getDependencies() {
		return Dependency.fromStrings("org.amdatu.web.rest.jaxrs", "org.amdatu.web.rest.doc", "javax.servlet", "jackson-core-asl",
				"jackson-mapper-asl");
	}

	@Override
	public boolean isInstalled() {
		return m_dependencyBuilder.hasDependencies(getDependencies());
	}

}
