/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.newplugin;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import org.amdatu.bootstrap.core.api.BndEditor;
import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.FullyQualifiedName;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Dependency;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.bootstrap.plugins.dependencymanager.api.DependencyManagerPlugin;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component(provides = Plugin.class)
public class CreatePlugin extends AbstractBasePlugin {

	@ServiceDependency
	private volatile Prompt m_prompt;

	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private volatile DependencyManagerPlugin m_dependencyManagerPlugin;

	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;
	
	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	@ServiceDependency
	private volatile BndEditor m_bndEditor;

	private volatile BundleContext m_bundleContext;

	@Command
	public void createPlugin() throws TemplateException, IOException {
		String pluginName = m_prompt.askString("What is the name of the plugin?");
		createPlugin(pluginName);
	}

	@Command
	public void createPlugin(String pluginName) throws TemplateException, IOException {
		m_navigator.createProject(pluginName);

		m_dependencyManagerPlugin.installAnnotationProcessor();

		m_dependencyBuilder.addDependencies(Dependency.fromStrings("org.amdatu.bootstrap.core", "org.amdatu.bootstrap.plugin.api",
				"../cnf/plugins/org.apache.felix.dependencymanager.annotation-3.1.1-SNAPSHOT.jar;version=file", "biz.aQute.bndlib",
				"org.amdatu.template.processor", "osgi.core"));

		FullyQualifiedName pluginComponent = m_prompt.askComponentName("What should be the fully qualified name of the plugin class?");

		createPluginClass(pluginName, pluginComponent);
		
		m_bndEditor.addPrivatePackage(m_navigator.getBndFile(), pluginComponent.getPackageName());
		
		createRunConfig(pluginName);
	}

	private void createPluginClass(String pluginName, FullyQualifiedName pluginComponent) throws TemplateException, IOException {
		URL templateUri = m_bundleContext.getBundle().getEntry("/templates/plugin.vm");

		TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
		TemplateContext context = m_templateEngine.createContext();
		context.put("componentName", pluginComponent.getClassName());
		context.put("packageName", pluginComponent.getPackageName());
		context.put("pluginName", pluginName);

		String template = processor.generateString(context);
		Path outputDir = m_navigator.getCurrentDir().resolve("src").resolve(pluginComponent.getPackageName().replaceAll("\\.", "/"));
		Path outputFile = outputDir.resolve(pluginComponent.getClassName() + ".java");
		Files.createDirectories(outputDir);

		System.out.println("Created file: " + outputFile);
		Files.write(outputFile, template.getBytes());
	}
	
	private void createRunConfig(String pluginName) throws TemplateException, IOException {
		URL templateUri = m_bundleContext.getBundle().getEntry("/templates/runconfig.vm");

		TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
		TemplateContext context = m_templateEngine.createContext();
		context.put("pluginName", pluginName);

		String template = processor.generateString(context);
		Path outputFile = m_navigator.getCurrentDir().resolve("run.bndrun");

		System.out.println("Created file: " + outputFile);
		Files.write(outputFile, template.getBytes());
	}

	@Override
	public String getName() {
		return "createplugin";
	}

	@Override
	public boolean isInstalled() {
		return true;
	}

}
