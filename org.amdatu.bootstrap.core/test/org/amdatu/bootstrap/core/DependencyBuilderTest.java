/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.service.event.EventAdmin;

@RunWith(MockitoJUnitRunner.class)
public class DependencyBuilderTest {
	@InjectMocks @Spy private DependencyBuilder m_dependencyBuilder = new DependencyBuilderImpl();
	@Mock private Navigator m_navigator;
	@Mock private ResourceManager m_resourceManager;
	@Mock private EventAdmin m_eventAdmin;
	@Captor private ArgumentCaptor<byte[]> contents; 
	
	@Before
	public void setup() throws URISyntaxException {
		URL resource = getClass().getClassLoader().getResource("bnd.bnd.test");
		when(m_navigator.getBndFile()).thenReturn(Paths.get(resource.toURI()));
	}
	
	@Test
	public void testAddDependencyWithVersion() {
		m_dependencyBuilder.addDependency("org.amdatu.test", "latest");
		
		verify(m_resourceManager).writeFile(any(Path.class), contents.capture());
		assertTrue(new String(contents.getValue()).contains("org.amdatu.test;version=latest"));
	}
	
	@Test
	public void testAddDependencyWithVersionRange() {
		m_dependencyBuilder.addDependency("org.amdatu.test", "[1.0.0,2.0.0)");
		
		verify(m_resourceManager).writeFile(any(Path.class), contents.capture());
		assertTrue(new String(contents.getValue()).contains("org.amdatu.test;version='[1.0.0,2.0.0)'"));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddDependencyWithInvalidVersionRange() {
		m_dependencyBuilder.addDependency("org.amdatu.test", "[1.0.0");
		verify(m_resourceManager).writeFile(any(Path.class), contents.capture());
	}
	
	@Test
	public void testAddDependencyWithoutVersion() {
		m_dependencyBuilder.addDependency("org.amdatu.test");
		
		verify(m_resourceManager).writeFile(any(Path.class), contents.capture());
		assertTrue(new String(contents.getValue()).contains("org.amdatu.test"));
	}

}
