/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.amdatu.bootstrap.core.api.DirectoryStructureBuilder;
import org.amdatu.bootstrap.core.api.InvalidProjectException;
import org.amdatu.bootstrap.core.api.InvalidWorkspaceException;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import aQute.bnd.build.Project;
import aQute.bnd.build.Workspace;

public class NavigatorImpl implements Navigator {
	private volatile EventAdmin m_eventAdmin;
	private volatile BundleContext m_bundleContext;
	private volatile TemplateEngine m_templateEngine;
	private volatile ResourceManager m_resourceManager;

	private Path m_workspaceDir;
	private Workspace m_workspace;
	private Project m_project;
	private Path m_projectDir;
	private Path m_currentDir;
	private Path m_previousDir;

	public void start() {
		Path lastLocationPath = m_bundleContext.getDataFile("lastlocation.txt").toPath();
		try {
			String lastKnownDir = new String(Files.readAllBytes(lastLocationPath));
			changeDir(Paths.get(lastKnownDir));
		} catch (IOException e) {
		}
	}

	@Override
	public void changeDir(Path newDir) {
		m_previousDir = m_currentDir;
		m_currentDir = newDir;
		Path lastLocationPath = m_bundleContext.getDataFile("lastlocation.txt").toPath();
		try {
			Files.write(lastLocationPath, m_currentDir.toString().getBytes());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		if (m_workspaceDir != null && !m_currentDir.startsWith(m_workspaceDir)) {
			m_workspace = null;
			m_project = null;
			m_projectDir = null;
			m_workspaceDir = null;

			sendEvent("org/amdatu/bootstrap/core/WORKSPACE_LEFT");
		}

		if (m_projectDir != null && !m_currentDir.startsWith(m_projectDir)) {
			m_project = null;
			m_projectDir = null;

			sendEvent("org/amdatu/bootstrap/core/PROJECT_LEFT");
		}

		for (File file : m_currentDir.toFile().listFiles()) {
			if (!m_currentDir.equals(m_workspaceDir) && file.isDirectory() && file.getName().equals("cnf")) {
				changeWorkspace(m_currentDir);
				break;
			} else if (file.isFile() && file.getName().endsWith(".bnd")) {
				changeProject(m_currentDir);
				break;
			}
		}
	}

	private void sendEvent(String topicName) {
		Map<String, Object> props = new HashMap<>();
		Event event = new Event(topicName, props);
		m_eventAdmin.sendEvent(event);
	}
	
	private void sendEvent(String topicName, String name) {
	    Map<String, Object> props = new HashMap<>();
	    props.put("projectname", name);
	    Event event = new Event(topicName, props);
	    m_eventAdmin.sendEvent(event);
	}

	@Override
	public Workspace getCurrentWorkspace() {
		return m_workspace;
	}

	@Override
	public Path getWorkspaceDir() {
		return m_workspaceDir;
	}

	@Override
	public Project getCurrentProject() {
		return m_project;
	}

	@Override
	public Path getProjectDir() {
		return m_projectDir;
	}

	@Override
	public Path getCurrentDir() {
		if (m_currentDir == null) {
			m_currentDir = getHomeDir();
		}

		return m_currentDir;
	}

	@Override
	public void createProject(String name) {
		if (m_project != null) {
			m_currentDir = m_workspaceDir;
		}

		try {
			TemplateProcessor processorProjectFile = m_templateEngine.createProcessor(m_bundleContext.getBundle().getEntry(
					"/templates/project.vm"));
			TemplateContext context = m_templateEngine.createContext();
			context.put("projectName", name);

			TemplateProcessor processorClasspathFile = m_templateEngine.createProcessor(m_bundleContext.getBundle().getEntry(
					"/templates/classpath.vm"));

			new DirectoryStructureBuilder(m_currentDir, name).withDirs("src", "test").withEmptyFiles("bnd.bnd")
					.withFile(".project", processorProjectFile.generateString(context))
					.withFile(".classpath", processorClasspathFile.generateString(context));
			changeDir(m_currentDir.resolve(name));
			sendEvent("org/amdatu/bootstrap/core/PROJECT_CREATED", name);

		} catch (TemplateException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void createWorkspace() throws IOException {
        URL buildFile = m_bundleContext.getBundle().getEntry("templates/cnf.zip");
        
        try(InputStream in = buildFile.openStream()) {
            Files.copy(in, getCurrentDir().resolve("cnf.zip"), StandardCopyOption.REPLACE_EXISTING);
        }
        File zipFile = getCurrentDir().resolve("cnf.zip").toFile();
        m_resourceManager.unzipFile(zipFile, getCurrentDir().toFile());
        
        // remove the unneeded file
        zipFile.delete();
        
        changeDir(getCurrentDir());
        sendEvent("org/amdatu/bootstrap/core/PROJECT_CREATED", "cnf");
    }

	@Override
	public Path getBndFile() {
		if (m_projectDir == null) {
			throw new IllegalStateException("Not in a project");
		}

		return m_projectDir.resolve("bnd.bnd");
	}

	@Override
    public List<Path> getBndRunFiles() {
	    File[] listFiles = m_currentDir.toFile().listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isFile() && pathname.toString().toLowerCase().endsWith(".bndrun");
            }
        });
	    
	    List<Path> result = new ArrayList<>();

	    for (File f : listFiles) {
	        result.add(f.toPath());
	    }
	    
        return result;
    }

    private void changeWorkspace(Path workspaceDir) {
		try {
			m_workspace = new Workspace(workspaceDir.toFile());
			m_workspaceDir = workspaceDir;

			sendEvent("org/amdatu/bootstrap/core/WORKSPACE_CHANGED");
		} catch (Exception e) {
			throw new InvalidWorkspaceException(e);
		}
	}

	private void changeProject(Path projectDir) {
		try {
			if (!projectDir.getParent().equals(m_workspaceDir)) {
				changeWorkspace(projectDir.getParent());
			}

			m_projectDir = projectDir;

			m_project = Workspace.getProject(projectDir.toFile());

			sendEvent("org/amdatu/bootstrap/core/PROJECT_CHANGED");
		} catch (Exception e) {
			throw new InvalidProjectException(e);
		}
	}

	@Override
	public List<File> listProjectBndFiles() {
		return Arrays.asList(m_projectDir.toFile().listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".bnd");
			}
		}));
	}

	@Override
	public List<Path> findWorkspaceRunConfigs() {
		try {
			RunConfigVisitor visitor = new RunConfigVisitor();
			Files.walkFileTree(m_workspaceDir, visitor);
			return visitor.getRunConfigsFound();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
    public String getProjectName() {
        Path currentProject = getProjectDir();
        if (currentProject != null) {
            return currentProject.getFileName().toString();
        }
        return null;
    }

    class RunConfigVisitor extends SimpleFileVisitor<Path> {
		private final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("regex:.*\\.bndrun");
		private final PathMatcher bndMatcher = FileSystems.getDefault().getPathMatcher("glob:/**/*.bnd");
		private final List<Path> runConfigsFound = new ArrayList<>();

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {

			if (matcher.matches(file)) {
				runConfigsFound.add(file);
			} else
				try {
					if(isIntegrationTestBnd(file)) {
						runConfigsFound.add(file);
					}
				} catch (IOException e) {
					//This should never throw an error, if it does something is broken.
					throw new RuntimeException(e);
				}
			
			return FileVisitResult.CONTINUE;
		}

		private boolean isIntegrationTestBnd(Path file) throws IOException {
			return bndMatcher.matches(file) && new String(Files.readAllBytes(file)).contains("Test-Cases");
		}

		public List<Path> getRunConfigsFound() {
			return runConfigsFound;
		}

	}

	@Override
	public Path getHomeDir() {
		return Paths.get(System.getProperty("user.home"));
	}
	
	@Override
	public Path getPreviousDir() {
		return m_previousDir != null ? m_previousDir : getHomeDir();
	}
}
