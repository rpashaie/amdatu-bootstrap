/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

import org.amdatu.bootstrap.core.api.BndEditor;
import org.amdatu.bootstrap.core.api.ResourceManager;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.properties.Document;

public class FileSystemBndEditor implements BndEditor {
	private volatile ResourceManager m_resourceManager;

	@Override
	public void addPrivatePackage(Path bndFile, String packageName) {
		try {
			BndEditModel model = new BndEditModel();
			model.loadFrom(bndFile.toFile());

			model.addPrivatePackage(packageName);

			saveModel(bndFile, model);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void saveModel(Path bndFile, BndEditModel model) throws IOException {
		Document document = new Document(new String(Files.readAllBytes(bndFile)));
		model.saveChangesTo(document);
		m_resourceManager.writeFile(bndFile, document.get().getBytes());
	}

	@Override
	public void addItestHeader(Path bndFile) {
		try {
			List<String> lines = Files.readAllLines(bndFile, Charset.defaultCharset());

			boolean foundTestCasesHeader = false;

			for (String line : lines) {
				if (line.startsWith("Test-Cases:")) {
					foundTestCasesHeader = true;
					break;
				}
			}

			if (!foundTestCasesHeader) {
				Files.write(bndFile, "\nTest-Cases: ${classes;CONCRETE;EXTENDS;junit.framework.TestCase}".getBytes(),
						StandardOpenOption.APPEND);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
