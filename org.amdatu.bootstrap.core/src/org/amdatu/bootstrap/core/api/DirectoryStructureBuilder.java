/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class DirectoryStructureBuilder {

	private Path m_baseDir;
	
	public DirectoryStructureBuilder(Path parentDir, String name) {
		m_baseDir = parentDir.resolve(name);
		try {
			Files.createDirectories(m_baseDir);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public DirectoryStructureBuilder withDirs(String... name) {
		for(String dir : name) {
			try {
				Files.createDirectories(m_baseDir.resolve(dir));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		
		return this;
	}
	
	public DirectoryStructureBuilder withEmptyFiles(String... name) {
		for (String file : name) {
			try {
				Files.createFile((m_baseDir.resolve(file)));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		
		return this;
	}
	
	public DirectoryStructureBuilder withFile(String name, String template) {
		try {
			Path newFile = Files.createFile((m_baseDir.resolve(name)));
			Files.write(newFile, template.getBytes());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		
		return this;
	}
}
