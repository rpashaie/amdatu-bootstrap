/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

import org.amdatu.bootstrap.core.api.BndEditor;
import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.PluginRegistry;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.template.processor.TemplateEngine;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.Converter;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventAdmin;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(BundleContext ctx, DependencyManager dm) throws Exception {

		dm.add(createComponent()
				.setInterface(PluginRegistry.class.getName(), null)
				.setImplementation(PluginRegistryImpl.class)
				.add(createServiceDependency().setService(Plugin.class)
						.setCallbacks("pluginAdded", "pluginRemoved"))
				.add(createBundleDependency().setCallbacks("pluginBundleAdded",
						"pluginBundleRemoved")));
		
		dm.add(createComponent()
				.setInterface(Navigator.class.getName(), null)
				.setImplementation(NavigatorImpl.class)
				.add(createServiceDependency().setService(EventAdmin.class)
						.setRequired(true))
				.add(createServiceDependency().setService(ResourceManager.class)
                        .setRequired(true))
				.add(createServiceDependency().setService(TemplateEngine.class)
						.setRequired(true)));

		dm.add(createComponent()
				.setInterface(DependencyBuilder.class.getName(), null)
				.setImplementation(DependencyBuilderImpl.class)
				.add(createServiceDependency().setService(EventAdmin.class)
				        .setRequired(true))
				.add(createServiceDependency().setService(Navigator.class)
						.setRequired(true))
				.add(createServiceDependency()
						.setService(ResourceManager.class).setRequired(true))
				.add(createServiceDependency().setService(Prompt.class)
						.setRequired(true)));

		dm.add(createComponent().setInterface(ResourceManager.class.getName(),
				null).setImplementation(DefaultResourceManager.class));
		
		dm.add(createComponent()
				.setInterface(Plugin.class.getName(), null)
				.setImplementation(CoreCommandsImpl.class)
				.add(createServiceDependency().setService(PluginRegistry.class))
				.add(createServiceDependency().setService(Navigator.class))
				.add(createServiceDependency().setService(Prompt.class))
				.add(createServiceDependency().setService(ResourceManager.class).setRequired(true))
				.add(createServiceDependency().setService(DependencyBuilder.class).setRequired(true)));
		
		dm.add(createComponent()
				.setInterface(BndEditor.class.getName(), null)
				.setImplementation(FileSystemBndEditor.class)
				.add(createServiceDependency().setService(ResourceManager.class).setRequired(true)));
		
		dm.add(createComponent()
				.setInterface(Converter.class.getName(), null)
				.setImplementation(Converters.class));
	}

}
