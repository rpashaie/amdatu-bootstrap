/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.PluginFilter;
import org.amdatu.bootstrap.core.api.PluginRegistry;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Dependency;
import org.amdatu.bootstrap.plugin.api.Plugin;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.VersionedClause;
import aQute.bnd.header.Attrs;
import aQute.bnd.properties.Document;

public class CoreCommandsImpl extends AbstractBasePlugin {

	private volatile PluginRegistry m_pluginRegistry;
	private volatile Navigator m_navigator;
	private volatile Prompt m_prompt;
	private volatile ResourceManager m_resourceManager;
	private volatile DependencyBuilder m_dependencyBuilder;

	@Command(description="List all registered plugins")
	public List<Plugin> listPlugins() {
		return m_pluginRegistry.listPlugins();
	}

	@Command(description="Show commands for a plugin")
	public void man(String... pluginName) {
	    if (pluginName.length == 0) {
	        System.out.println("Please enter a name of a plugin to show the commands");
	        return;
	    }
		Plugin plugin = m_pluginRegistry.getPlugin(pluginName[0]);
		
		Method[] declaredMethods = plugin.getClass().getDeclaredMethods();
		for (Method method : declaredMethods) {
			if (method.isAnnotationPresent(Command.class)) {
				Command command = method.getAnnotation(Command.class);
				System.out.println(method.getName() + " - " + command.description());
			}
		}
	}
	
	@Command(description="Change directory to user's home directory")
	public File cd() {
		Path dir = m_navigator.getHomeDir();
		m_navigator.changeDir(dir);
		return dir.toFile();
	}
	
	@Command(description="Change directory")
	public File cd(String dirName) throws IOException {
		Path currentDir = m_navigator.getCurrentDir();
		Path newDir;
		
		if (dirName.equals("..")) {
			newDir = currentDir.getParent();
		}
		else if (dirName.equals("-")) {
			newDir = m_navigator.getPreviousDir();
		}
		else if (dirName.equals("~")) {
			newDir = m_navigator.getHomeDir();
		}
		else if (dirName.startsWith("~" + File.separator)) {
	        newDir = m_navigator.getHomeDir().resolve(dirName.substring(2));
		}
		else {
		    newDir = currentDir.resolve(dirName);
		}
		
		if (newDir.toFile().exists() && newDir.toFile().isDirectory()) {
			m_navigator.changeDir(newDir);
			return newDir.toFile();
		}
		else {
			throw new IOException("Invalid directory " + newDir);
		}
	}
	
	@Command(description="Print working directory")
	public File pwd() {
		return m_navigator.getCurrentDir().toFile();
	}
	
	@Command(description="List directory contents")
	public List<File> ls(String... args) {
		FileFilter filter = null;
		boolean recursive = false;
		boolean filesOnly = false;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-R")) {
				recursive = true;
			}
			else if (args[i].equals("-F")) {
				filesOnly = true;
			}
			else {
				final String pattern = args[i];
				filter = new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						String string = pathname.toString();
						return string.matches(pattern);
					}
				};
			}
		}
		
		File dir = m_navigator.getCurrentDir().toFile();
		File[] files;
		if (recursive) {
			return listRecursiveFiles(dir, filter, filesOnly);
		}
		else {
			files = (filter == null ? dir.listFiles() : dir.listFiles(filter));
			return Arrays.asList(files);
		}
	}
	
	private List<File> listRecursiveFiles(File dir, FileFilter filter, boolean filesOnly) {
		List<File> list = new ArrayList<>();
		listRecursiveFiles(dir, list, filter, filesOnly);
		return list;
	}
	
	private void listRecursiveFiles(File dir, List<File> list, FileFilter filter, boolean filesOnly) {
		File[] files = dir.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				listRecursiveFiles(file, list, filter, filesOnly);
				if (!filesOnly) {
					list.add(file);
				}
			}
		}
		files = (filter == null ? dir.listFiles() : dir.listFiles(filter));
		for (File file : files) {
			if (file.isFile()) {
				list.add(file);
			}
		}
	}

	@Command(description = "Creates a folder in the current directory")
    public File mkdir(String folderName) throws IOException {
	    String pathname = m_navigator.getCurrentDir().toString() + "/" + folderName;
        File path = new File(pathname);
		if (path.isDirectory()) {
			return path;
		}
		else {
			if (path.mkdirs()) {
				return path;
			} else {
				throw new IOException("Could not create folder " + pathname);
			}
		}
    }
	
	@Command(description = "Removes a file or folder (recursively)")
	public File rm(String name) throws IOException {
	    File file = m_navigator.getCurrentDir().resolve(name).toFile();
		m_resourceManager.delete(file);
		return file;
	}
	
	@Command
	public void createProject(String... args) {
	    String name;
	    if (args.length > 0) {
	        name = args[0];
	    } else {
	        name = m_prompt.askString("Name of the project?");
	    }
		m_navigator.createProject(name);
	}
	
	@Command
	public void createWorkspace() throws IOException {
	    if (!m_prompt.askBoolean("Create a workspace in " + m_navigator.getCurrentDir(), true)) {
	        return;
	    }
	    m_navigator.createWorkspace();
	}

    @Command
	public void install() throws Exception {
		install(null);
	}
	
	@Command
	public void install(String name) throws Exception {
	    Path workspaceDir = m_navigator.getWorkspaceDir();
	    if (workspaceDir == null) {
	    	throw new IllegalStateException("You can only install plugins in a workspace or a project");
	    }
	    final boolean isProject = m_navigator.getProjectDir() != null;
		List<Plugin> plugins = m_pluginRegistry.listPlugins(new PluginFilter() {
			@Override
			public boolean filter(Plugin plugin) {
				return ((isProject && plugin.isProjectInstallAllowed()) || (plugin.isWorkspaceInstallAllowed())) 
				                && !plugin.isInstalled();
			}
		});
		
		if (plugins.isEmpty()) {
			throw new IllegalStateException("Nothing to install at this level at this time");
		}
		
		Plugin pluginToInstall;
		if(name != null) {
			pluginToInstall = m_pluginRegistry.getPlugin(name);
		}
		else {
			pluginToInstall = m_prompt.askChoice("Which plugin do you want to install?", 0, plugins);
		}
		
		Collection<Dependency> dependencies = pluginToInstall.getDependencies();
		
		m_dependencyBuilder.addDependencies(dependencies);
		
		pluginToInstall.install();
	}
	
	@Command
	public void addRunconfig() throws IOException {
		addRunConfig(null);
	}
 	
	@Command
	public void addRunConfig(String name) throws IOException {
		Plugin pluginToInstall;
		if(name == null) {
			List<Plugin> plugins = m_pluginRegistry.listPlugins();
			pluginToInstall = m_prompt.askChoice("Which run requirements do you want to install?", 0, plugins);
		} else {
			pluginToInstall = m_pluginRegistry.getPlugin(name);
		}
		
		Collection<Dependency> runRequirements = pluginToInstall.getRunRequirements();
		Path path = m_prompt.askChoice("Which run config do you want to add to?", 0, m_navigator.findWorkspaceRunConfigs());
		
		BndEditModel model = new BndEditModel(); 
		
		model.loadFrom(path.toFile());
		
		List<VersionedClause> runBundles = model.getRunBundles();
		if(runBundles == null) {
			runBundles = new ArrayList<>();
		}
		
		for (Dependency dependency : runRequirements) {
			addRunBundle(dependency, runBundles);
		}
		
		model.setRunBundles(runBundles);
		
		String bndContents = new String(Files.readAllBytes(path));
		Document document = new Document(bndContents);
		model.saveChangesTo(document);
		
		m_resourceManager.writeFile(path, document.get().getBytes());
	}
	
	@Command
	public List<Plugin> listInstalled() {
	    Path workspaceDir = m_navigator.getWorkspaceDir();
        if (workspaceDir == null) {
        	throw new IllegalStateException("You can only install plugins in a workspace or a project");
        }
        final boolean isProject = m_navigator.getProjectDir() != null;
		return m_pluginRegistry.listPlugins(new PluginFilter() {
			@Override
			public boolean filter(Plugin plugin) {
			    return ((isProject && plugin.isProjectInstallAllowed()) || (plugin.isWorkspaceInstallAllowed())) 
                                && plugin.isInstalled();
			}
		});
	}
	
	private void addRunBundle(Dependency dependency, List<VersionedClause> runBundles) {
		for (VersionedClause versionedClause : runBundles) {
			if(versionedClause.getName().equals(dependency.getBsn())) {
				System.out.println("Skipping " + dependency.getBsn() + " since it was already in the run config");
				return;
			}
		}
		
		runBundles.add(new VersionedClause(dependency.getBsn(), new Attrs()));		
	}
	
	@Override
	public String getName() {
		return "core";
	}
	
	/*******************************************************************
     * This plugin can't be installed as not something you can install *
     *******************************************************************/

    @Override
    public boolean isProjectInstallAllowed() {
        return false;
    }

    @Override
    public boolean isWorkspaceInstallAllowed() {
        return false;
    }

    @Override
    public boolean isInstalled() {
        return true;
    }
}
