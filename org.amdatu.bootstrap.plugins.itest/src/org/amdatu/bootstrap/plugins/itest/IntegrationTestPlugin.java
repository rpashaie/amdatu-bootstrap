package org.amdatu.bootstrap.plugins.itest;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import org.amdatu.bootstrap.core.api.BndEditor;
import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.FullyQualifiedName;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Dependency;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component(provides = Plugin.class)
public class IntegrationTestPlugin extends AbstractBasePlugin {

	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private volatile BndEditor m_bndEditor;

	@ServiceDependency
	private volatile Prompt m_prompt;

	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;

	private volatile BundleContext m_bundleContext;

	@Command(description = "Creates a new project for integration tests")
	public void createItestProject(String... args) throws TemplateException, IOException {
		String projectName;
		if (args.length > 0) {
			projectName = args[0];
		} else {
			projectName = m_prompt.askString("Name of the project?");
		}

		m_navigator.createProject(projectName);

		m_dependencyBuilder.addDependencies(Dependency.fromStrings("org.amdatu.bndtools.test", "org.apache.felix.dependencymanager",
				"junit.osgi", "osgi.core", "osgi.cmpn"));

		m_dependencyBuilder.addRunDependency(Dependency.fromStrings("org.apache.felix.dependencymanager",
				"org.apache.felix.dependencymanager.runtime", "org.apache.felix.configadmin", "org.apache.felix.metatype",
				"org.apache.felix.log", "org.apache.felix.eventadmin", "org.amdatu.bndtools.test", "junit.osgi"));

		m_bndEditor.addItestHeader(m_navigator.getBndFile());

		Files.write(m_navigator.getBndFile(), "\n-runfw:org.apache.felix.framework;version='[4.2.1,4.2.1]'\n-runee:JavaSE-1.7".getBytes(),
				StandardOpenOption.APPEND);

		String packageName = createTestTemplate();

		m_bndEditor.addPrivatePackage(m_navigator.getBndFile(), packageName);
	}

	private String createTestTemplate() throws TemplateException, IOException {
		FullyQualifiedName serviceToTest = m_prompt.askComponentName("Which service do you want to test?");
		URL templateUri = m_bundleContext.getBundle().getEntry("/templates/itest.vm");
		TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
		TemplateContext context = m_templateEngine.createContext();
		context.put("packageUnderTest", serviceToTest.getPackageName());
		String packageName = serviceToTest.getPackageName() + ".test";
		context.put("package", packageName);
		context.put("serviceName", serviceToTest.getClassName());
		String testClassName = serviceToTest.getClassName() + "Test";
		context.put("testName", testClassName);

		String template = processor.generateString(context);
		Path outputDir = m_navigator.getCurrentDir().resolve("src").resolve(packageName.replaceAll("\\.", "/"));
		Path outputFile = outputDir.resolve(testClassName + ".java");
		Files.createDirectories(outputDir);

		System.out.println("Created file: " + outputFile);
		Files.write(outputFile, template.getBytes());
		return packageName;
	}

	@Override
	public String getName() {
		return "itest";
	}

	@Override
	public boolean isInstalled() {
		return true;
	}

}
